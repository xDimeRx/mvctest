﻿using MVCtest.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVCtest.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public JsonResult getOKEVD2(string needle)
        {
            var cars = new List<Car>
            {
                new Car
                {
                    code = "V",
                    name = "Ваз"
                },
                new Car
                {
                    code = "F",
                    name = "Форд"
                },
                new Car
                {
                    code = "M",
                    name = "Мазда"
                },
                new Car
                {
                    code = "S",
                    name = "Субару"
                },
                new Car
                {
                    code = "N",
                    name = "Ниссан"
                },
                new Car
                {
                    code = "T",
                    name = "Тойота"
                },
                new Car
                {
                    code = "O",
                    name = "Опель"
                },
                new Car
                {
                    code = "A",
                    name = "Ауди"
                },
                new Car
                {
                    code = "P",
                    name = "Порше"
                },
                new Car
                {
                    code = "L",
                    name = "Ламборджини"
                }
            };

            if (needle != null)
            {
                var matched = cars.FirstOrDefault(x => x.name.Contains(needle));
                if (matched == null)
                {
                    return Json("null", JsonRequestBehavior.AllowGet);
                }
                else
                { 
                    return Json(matched, JsonRequestBehavior.AllowGet);
                }
            }else
            {
                return Json("null", JsonRequestBehavior.AllowGet);
            }

        }
    }
}