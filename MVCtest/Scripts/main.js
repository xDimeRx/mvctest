﻿function countNDS( val ) {
    if ($('[name=nds-type]').length == 0) {
        console.log('Отсутствует поле с вариантами НДС!');
        return false;
    }

    var nds = $('[name=nds-type]').val();
    
    if (nds == 0) {
        $('#nds-all').val(0.00);
        $('#without-nds').val(val);
    } else {
        var nds_summ = val / (1 + nds);
        var summ_wo_nds = val - nds_summ;
        $('#nds-all').val((nds_summ).toFixed(2));
        $('#without-nds').val((summ_wo_nds).toFixed(2));
    }
}

$(document).ready(function () {
    $('input[name=price]').change(function () {
        if ($(this).val() == 'amount') {
            $('.start-end-price').hide();
        } else {
            $('.start-end-price').show();
        }
    });

    $('input[name=start-end-price]').change(function () {
        if ($(this).prop('checked')) {
            $('.form__row_price').hide();
        } else {
            $('.form__row_price').show();
        }
    })

    $('input[name=nds-state]').change(function () {
        if ($(this).prop('checked')) {
            $('.nds-controls').show();
        } else {
            $('.nds-controls').hide();
        }
    })

    $('[name=name]').keyup(function () {
        if ($(this).val().length != 1) {
            return;
        }
        var url = "/Home/getOKEVD2/";
        var data = {
            needle:$(this).val()
        };
        $.get(url, data, function (data) {
            if (data != 'null') {
                var data_string = "<option value=" + data.code + ">" + data.code + " - " + data.name + "</option>";
                $('[name=okevd2]').html(data_string);
                $('[name=okpd2]').html(data_string);
            }
        });
    })

    $('input[name=start-end-ammount]').change(function () {
        var val = $(this).val();
        countNDS(val);
    });

    $('[name=nds-type]').change(function () {
        var val = $('[name=start-end-ammount]').val();
        countNDS(val);
    })

    $('[name=okevd2]').select2({
        minimumInputLength: 1,
        containerCssClass:"form-control",
        width: '100%',
        ajax: {
            cache: false,
            url: "/Home/getOKEVD2/",
            data: function (params) {
                return { needle: params.term };
            },
            dataType: 'json',
            type: "GET",
            processResults: function (data) {
                if (data == 'null') {
                    return { results: null };
                }
                return {
                    results: [{ text:data.code+" - "+data.name,id:data.code }]
                };
            }
        }
    })

})